﻿using UnityEngine;
using System.Collections;

public class sebell : MonoBehaviour {

    public AudioSource source;
    public AudioClip sebel;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.LeftArrow) ||
            Input.GetKeyDown(KeyCode.UpArrow)    || Input.GetKeyDown(KeyCode.DownArrow))
        {
            source.PlayOneShot(sebel);
        }

	}
}
