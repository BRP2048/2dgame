﻿#pragma strict
var blockPrefab : Transform;
var badeffect : Transform;
var goodeffect : Transform;
var perfecteffect : Transform; 
var keyinputtime;
var Lane3time = new Array();
var Lane3LS = new Array();
var Lane3LE = new Array();
var Success : AudioClip;
var Fail : AudioClip;
var Readcount = 0;
var NortsAllcount = 0;
var stf = 0.0f;
var adjustcount = 0;
var arom = 0.0f;
var aroms = 0.0f;
var aromls = 0.0f;
var aromle = 0.0f;
public static var Nowlongmode = false;
var LScount = 0;
var LEcount = 0;
var stfLS = 0.0f;
var stfLE = 0.0f;
var SNAllcount = 0;
var LSAllcount = 0;
var LEAllcount = 0;

function Start () {

}
function Update () {
if(Scores_oper.sector == 2)
{
	while(Readcount < ReadOutNorts.Lane3.length)
	{
		Lane3time[Readcount] = ReadOutNorts.Lane3[Readcount];
		Readcount = Readcount + 1;
	}
	Readcount = 0;
	while(Readcount < ReadOutNorts.Lane3LS.length)
	{
		Lane3LS[Readcount] = ReadOutNorts.Lane3LS[Readcount];
		Readcount = Readcount + 1;
	}
	Readcount = 0;
	while(Readcount < ReadOutNorts.Lane3LE.length)
	{
		Lane3LE[Readcount] = ReadOutNorts.Lane3LE[Readcount];
		Readcount = Readcount + 1;
	}
	NortsAllcount = Lane3time.length + Lane3LS.length + Lane3LE.length;
	SNAllcount = Lane3time.length;
	LSAllcount = Lane3LS.length;
	LEAllcount = Lane3LE.length;
	Scores_oper.sector3_Lane3 = true;
}
//シングルノーツ処理
if(Scores_oper.sector == 3)
{
	if(Scores_oper.Automode == false)
	{
	if(SNAllcount != adjustcount)
	{
		if(Nowlongmode == false)
		{
		stf = float.Parse(Lane3time[adjustcount]) + 0.0f;
		stf = stf / 1000f;
		stf = stf + Scores_oper.waittime + Scores_oper.juster + Scores_oper.starttime;
		//判定開始値
		if(Time.timeSinceLevelLoad - stf > Scores_oper.badjudge)
		{
			if(Scores_oper.debug_mode == true)
			{
				Debug.Log(Time.time - stf);
				Debug.Log("BAD");
			}
			//GetComponent.<AudioSource>().clip = Fail;
			//GetComponent(AudioSource).Play();
			adjustcount = adjustcount + 1;
			Scores_oper.bad_count = Scores_oper.bad_count + 1;
			Instantiate(badeffect,transform.position,transform.rotation);
			Scores_oper.score = Scores_oper.score + Scores_oper.score_bad_perse * Scores_oper.OnenortScore;
			Scores_oper.NortsID_del_Lane3 = Scores_oper.NortsID_del_Lane3 + 1;
			Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugeBadParse;
			Scores_oper.lastjudge = "Bad";
			Scores_oper.combo = 0;
		}
		if(Input.GetKeyDown(KeyCode.UpArrow)||Input.GetKeyDown(KeyCode.O))
		{
			arom = Time.timeSinceLevelLoad - stf;
			if(Scores_oper.debug_mode == true)
			{
				Debug.Log(arom);
			}
			if(arom <= Scores_oper.goodjudge && arom > Scores_oper.perfectjudge)
			{
				//LatEAR
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("LatGOOD");
				}
				GetComponent.<AudioSource>().clip = Success;
				GetComponent(AudioSource).Play();
				adjustcount = adjustcount + 1;
				Scores_oper.combo = Scores_oper.combo + 1;
				Instantiate(goodeffect,transform.position,transform.rotation);
				Scores_oper.good_count = Scores_oper.good_count + 1;
				Scores_oper.score = Scores_oper.score + Scores_oper.score_good_perse * Scores_oper.OnenortScore;
				Scores_oper.NortsID_del_Lane3 = Scores_oper.NortsID_del_Lane3 + 1;
				Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugeGoodParse;
				Scores_oper.lastjudge = "Good";
			}
			if(arom <= Scores_oper.perfectjudge && arom > 0.0)
			{
				//LatPF
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("LatPERFECT");
				}
				GetComponent.<AudioSource>().clip = Success;
				GetComponent(AudioSource).Play();
				adjustcount = adjustcount + 1;
				Scores_oper.combo = Scores_oper.combo + 1;
				Instantiate(perfecteffect,transform.position,transform.rotation);
				Scores_oper.perfect_count = Scores_oper.perfect_count + 1;
				Scores_oper.score = Scores_oper.score + Scores_oper.score_perfect_perse * Scores_oper.OnenortScore;
				Scores_oper.NortsID_del_Lane3 = Scores_oper.NortsID_del_Lane3 + 1;
				Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugePerfectParse;
				Scores_oper.lastjudge = "Perfect";
			}
			if(arom <= 0.0 && arom > -1.0f * Scores_oper.perfectjudge)
			{
				//EalPF
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("EalPERFECT");
				}
				GetComponent.<AudioSource>().clip = Success;
				GetComponent(AudioSource).Play();
				adjustcount = adjustcount + 1;
				Scores_oper.combo = Scores_oper.combo + 1;
				Instantiate(perfecteffect,transform.position,transform.rotation);
				Scores_oper.perfect_count = Scores_oper.perfect_count + 1;
				Scores_oper.score = Scores_oper.score + Scores_oper.score_perfect_perse * Scores_oper.OnenortScore;
				Scores_oper.NortsID_del_Lane3 = Scores_oper.NortsID_del_Lane3 + 1;
				Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugePerfectParse;
				Scores_oper.lastjudge = "Perfect";
			}
			if(arom <= -1.0f * Scores_oper.perfectjudge && arom > -1.0f * Scores_oper.goodjudge)
			{
				//EalEAR
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("EalGOOD");
				}
				GetComponent.<AudioSource>().clip = Success;
				GetComponent(AudioSource).Play();
				adjustcount = adjustcount + 1;
				Scores_oper.combo = Scores_oper.combo + 1;
				Instantiate(goodeffect,transform.position,transform.rotation);
				Scores_oper.good_count = Scores_oper.good_count + 1;
				Scores_oper.score = Scores_oper.score + Scores_oper.score_good_perse * Scores_oper.OnenortScore;
				Scores_oper.NortsID_del_Lane3 = Scores_oper.NortsID_del_Lane3 + 1;
				Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugeGoodParse;
				Scores_oper.lastjudge = "Good";
			}
			if(arom <= -1.0f * Scores_oper.airjudge)
			{
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("AIR");
				}
				//GetComponent.<AudioSource>().clip = Fail;
				//GetComponent(AudioSource).Play();
			}
			}
		}
	}
	//ロング始点
	if(LSAllcount != LScount)
	{
		if(Nowlongmode == false)
		{
		stfLS = float.Parse(Lane3LS[LScount]) + 0.0f;
		stfLS = stfLS / 1000f;
		stfLS = stfLS + Scores_oper.waittime + Scores_oper.juster + Scores_oper.starttime;
		//判定開始値
		if(Time.timeSinceLevelLoad - stfLS > Scores_oper.badjudge)
		{
			if(Scores_oper.debug_mode == true)
			{
				Debug.Log(Time.time - stfLS);
				Debug.Log("BAD");
			}
			//GetComponent.<AudioSource>().clip = Fail;
			//GetComponent(AudioSource).Play();
			LScount = LScount + 1;
			Scores_oper.bad_count = Scores_oper.bad_count + 2;
			Instantiate(badeffect,transform.position,transform.rotation);
			Scores_oper.score = Scores_oper.score + Scores_oper.score_bad_perse + Scores_oper.score_bad_perse  * Scores_oper.OnenortScore;
			Scores_oper.NortsID_del_Lane3_L = Scores_oper.NortsID_del_Lane3_L + 1;
			Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugeBadParse;
			Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugeBadParse;
			LEcount = LEcount + 1;
			Scores_oper.lastjudge = "Bad";
			Scores_oper.combo = 0;
		}
		if(Input.GetKeyDown(KeyCode.UpArrow)||Input.GetKeyDown(KeyCode.O))
		{
			arom = Time.timeSinceLevelLoad - stfLS;
			if(Scores_oper.debug_mode == true)
			{
				Debug.Log(arom);
			}
			if(arom <= Scores_oper.goodjudge && arom > Scores_oper.perfectjudge)
			{
				//LatEAR
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("LatGOOD");
				}
				GetComponent.<AudioSource>().clip = Success;
				GetComponent(AudioSource).Play();
				LScount = LScount + 1;
				Scores_oper.combo = Scores_oper.combo + 1;
				Instantiate(goodeffect,transform.position,transform.rotation);
				Scores_oper.good_count = Scores_oper.good_count + 1;
				Scores_oper.score = Scores_oper.score + Scores_oper.score_good_perse * Scores_oper.OnenortScore;
				Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugeGoodParse;
				Scores_oper.lastjudge = "Good";
				Nowlongmode = true;
			}
			if(arom <= Scores_oper.perfectjudge && arom > 0.0)
			{
				//LatPF
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("LatPERFECT");
				}
				GetComponent.<AudioSource>().clip = Success;
				GetComponent(AudioSource).Play();
				LScount = LScount + 1;
				Scores_oper.combo = Scores_oper.combo + 1;
				Instantiate(perfecteffect,transform.position,transform.rotation);
				Scores_oper.perfect_count = Scores_oper.perfect_count + 1;
				Scores_oper.score = Scores_oper.score + Scores_oper.score_perfect_perse * Scores_oper.OnenortScore;
				Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugePerfectParse;
				Scores_oper.lastjudge = "Perfect";
				Nowlongmode = true;
			}
			if(arom <= 0.0 && arom > -1.0f * Scores_oper.perfectjudge)
			{
				//EalPF
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("EalPERFECT");
				}
				GetComponent.<AudioSource>().clip = Success;
				GetComponent(AudioSource).Play();
				LScount = LScount + 1;
				Scores_oper.combo = Scores_oper.combo + 1;
				Instantiate(perfecteffect,transform.position,transform.rotation);
				Scores_oper.perfect_count = Scores_oper.perfect_count + 1;
				Scores_oper.score = Scores_oper.score + Scores_oper.score_perfect_perse * Scores_oper.OnenortScore;
				Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugePerfectParse;
				Scores_oper.lastjudge = "Perfect";
				Nowlongmode = true;
			}
			if(arom <= -1.0f * Scores_oper.perfectjudge && arom > -1.0f * Scores_oper.goodjudge)
			{
				//EalEAR
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("EalGOOD");
				}
				GetComponent.<AudioSource>().clip = Success;
				GetComponent(AudioSource).Play();
				LScount = LScount + 1;
				Scores_oper.combo = Scores_oper.combo + 1;
				Instantiate(goodeffect,transform.position,transform.rotation);
				Scores_oper.good_count = Scores_oper.good_count + 1;
				Scores_oper.score = Scores_oper.score + Scores_oper.score_good_perse * Scores_oper.OnenortScore;
				Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugeGoodParse;
				Scores_oper.lastjudge = "Good";
				Nowlongmode = true;
			}
			if(arom <= -1.0f * Scores_oper.airjudge)
			{
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("AIR");
				}
				//GetComponent.<AudioSource>().clip = Fail;
				//GetComponent(AudioSource).Play();
			}
			}
		}
	}
	//ロング終点
	if(LEAllcount != LEcount)
	{
		if(Nowlongmode == true)
		{
		stfLE = float.Parse(Lane3LE[LEcount]) + 0.0f;
		stfLE = stfLE / 1000f;
		stfLE = stfLE + Scores_oper.waittime + Scores_oper.juster + Scores_oper.starttime;
		//判定開始値
		if(Time.timeSinceLevelLoad - stfLE > Scores_oper.LNbadjudge)
		{
			if(Scores_oper.debug_mode == true)
			{
				Debug.Log(Time.time - stf);
				Debug.Log("BAD");
			}
			//GetComponent.<AudioSource>().clip = Fail;
			//GetComponent(AudioSource).Play();
			LEcount = LEcount + 1;
			Scores_oper.bad_count = Scores_oper.bad_count + 1;
			Instantiate(badeffect,transform.position,transform.rotation);
			Scores_oper.score = Scores_oper.score + Scores_oper.score_bad_perse * Scores_oper.OnenortScore;
			Scores_oper.NortsID_del_Lane3_L = Scores_oper.NortsID_del_Lane3_L + 1;
			Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugeBadParse;
			Scores_oper.lastjudge = "Bad";
			Scores_oper.combo = 0;
			Nowlongmode = false;
		}
		if(Input.GetKeyUp(KeyCode.UpArrow)||Input.GetKeyUp(KeyCode.O))
		{
			arom = Time.timeSinceLevelLoad - stfLE;
			if(Scores_oper.debug_mode == true)
			{
				Debug.Log(arom);
			}
			if(arom <= Scores_oper.LNgoodjudge && arom > Scores_oper.LNperfectjudge)
			{
				//LatEAR
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("LatGOOD");
				}
				GetComponent.<AudioSource>().clip = Success;
				GetComponent(AudioSource).Play();
				LEcount = LEcount + 1;
				Scores_oper.combo = Scores_oper.combo + 1;
				Instantiate(goodeffect,transform.position,transform.rotation);
				Scores_oper.good_count = Scores_oper.good_count + 1;
				Scores_oper.score = Scores_oper.score + Scores_oper.score_good_perse * Scores_oper.OnenortScore;
				Scores_oper.NortsID_del_Lane3_L = Scores_oper.NortsID_del_Lane3_L + 1;
				Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugeGoodParse;
				Scores_oper.lastjudge = "Good";
				Nowlongmode = false;
			}
			if(arom <= Scores_oper.LNperfectjudge && arom > 0.0)
			{
				//LatPF
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("LatPERFECT");
				}
				GetComponent.<AudioSource>().clip = Success;
				GetComponent(AudioSource).Play();
				LEcount = LEcount + 1;
				Scores_oper.combo = Scores_oper.combo + 1;
				Instantiate(perfecteffect,transform.position,transform.rotation);
				Scores_oper.perfect_count = Scores_oper.perfect_count + 1;
				Scores_oper.score = Scores_oper.score + Scores_oper.score_perfect_perse * Scores_oper.OnenortScore;
				Scores_oper.NortsID_del_Lane3_L = Scores_oper.NortsID_del_Lane3_L + 1;
				Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugePerfectParse;
				Scores_oper.lastjudge = "Perfect";
				Nowlongmode = false;
			}
			if(arom <= 0.0 && arom > -1.0f * Scores_oper.LNperfectjudge)
			{
				//EalPF
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("EalPERFECT");
				}
				GetComponent.<AudioSource>().clip = Success;
				GetComponent(AudioSource).Play();
				LEcount = LEcount + 1;
				Scores_oper.combo = Scores_oper.combo + 1;
				Instantiate(perfecteffect,transform.position,transform.rotation);
				Scores_oper.perfect_count = Scores_oper.perfect_count + 1;
				Scores_oper.score = Scores_oper.score + Scores_oper.score_perfect_perse * Scores_oper.OnenortScore;
				Scores_oper.NortsID_del_Lane3_L = Scores_oper.NortsID_del_Lane3_L + 1;
				Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugePerfectParse;
				Scores_oper.lastjudge = "Perfect";
				Nowlongmode = false;
			}
			if(arom <= -1.0f * Scores_oper.LNperfectjudge && arom > -1.0f * Scores_oper.LNgoodjudge)
			{
				//EalEAR
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("EalGOOD");
				}
				GetComponent.<AudioSource>().clip = Success;
				GetComponent(AudioSource).Play();
				LEcount = LEcount + 1;
				Scores_oper.combo = Scores_oper.combo + 1;
				Instantiate(goodeffect,transform.position,transform.rotation);
				Scores_oper.good_count = Scores_oper.good_count + 1;
				Scores_oper.score = Scores_oper.score + Scores_oper.score_good_perse * Scores_oper.OnenortScore;
				Scores_oper.NortsID_del_Lane3_L = Scores_oper.NortsID_del_Lane3_L + 1;
				Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugeGoodParse;
				Scores_oper.lastjudge = "Good";
				Nowlongmode = false;
			}
			if(arom <= -1.0f * Scores_oper.LNairjudge)
			{
				if(Scores_oper.debug_mode == true)
				{
					Debug.Log("AIR");
				}
				//GetComponent.<AudioSource>().clip = Fail;
				//GetComponent(AudioSource).Play();
				LEcount = LEcount + 1;
				Scores_oper.bad_count = Scores_oper.bad_count + 1;
				Instantiate(badeffect,transform.position,transform.rotation);
				Scores_oper.score = Scores_oper.score + Scores_oper.score_bad_perse * Scores_oper.OnenortScore;
				Scores_oper.NortsID_del_Lane3_L = Scores_oper.NortsID_del_Lane3_L + 1;
				Scores_oper.lastjudge = "Bad";
				Scores_oper.combo = 0;
				Nowlongmode = false;
			}
		}
		}
	}
	}
	else if(Scores_oper.Automode == true)
	{
		if(SNAllcount != adjustcount)
		{
			if(Nowlongmode == false)
			{
				stf = float.Parse(Lane3time[adjustcount]) + 0.0f;
				stf = stf / 1000f;
				stf = stf + Scores_oper.waittime + Scores_oper.juster + Scores_oper.starttime;
				aroms = Time.timeSinceLevelLoad - stf;
				if(aroms <= 0.02f && aroms > -0.02f)
				{
					//LatPF
					if(Scores_oper.debug_mode == true)
					{
						Debug.Log("LatPERFECT");
					}
					GetComponent.<AudioSource>().clip = Success;
					GetComponent(AudioSource).Play();
					adjustcount = adjustcount + 1;
					Scores_oper.combo = Scores_oper.combo + 1;
					Instantiate(perfecteffect,transform.position,transform.rotation);
					Scores_oper.perfect_count = Scores_oper.perfect_count + 1;
					Scores_oper.score = Scores_oper.score + Scores_oper.score_perfect_perse * Scores_oper.OnenortScore;
					Scores_oper.NortsID_del_Lane3 = Scores_oper.NortsID_del_Lane3 + 1;
					Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugePerfectParse;
					Scores_oper.lastjudge = "Perfect";
				}
			}
		}
		if(LSAllcount != LScount)
		{
			if(Nowlongmode == false)
			{
				stfLS = float.Parse(Lane3LS[LScount]) + 0.0f;
				stfLS = stfLS / 1000f;
				stfLS = stfLS + Scores_oper.waittime + Scores_oper.juster + Scores_oper.starttime;
				aromls = Time.timeSinceLevelLoad - stfLS;
				if(aromls <= 0.02f && aromls > -0.02f)
				{
					//LatPF
					if(Scores_oper.debug_mode == true)
					{
						Debug.Log("LatPERFECT");
					}
					GetComponent.<AudioSource>().clip = Success;
					GetComponent(AudioSource).Play();
					LScount = LScount + 1;
					Scores_oper.combo = Scores_oper.combo + 1;
					Instantiate(perfecteffect,transform.position,transform.rotation);
					Scores_oper.perfect_count = Scores_oper.perfect_count + 1;
					Scores_oper.score = Scores_oper.score + Scores_oper.score_perfect_perse * Scores_oper.OnenortScore;
					Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugePerfectParse;
					Scores_oper.lastjudge = "Perfect";
					Nowlongmode = true;
				}
			}
		}
		if(LEAllcount != LEcount)
		{
			if(Nowlongmode == true)
			{
				stfLE = float.Parse(Lane3LE[LEcount]) + 0.0f;
				stfLE = stfLE / 1000f;
				stfLE = stfLE + Scores_oper.waittime + Scores_oper.juster + Scores_oper.starttime;
				aromle = Time.timeSinceLevelLoad - stfLE;
				if(aromle <= 0.02f && aromle > -0.02f)
				{
					//LatPF
					if(Scores_oper.debug_mode == true)
					{
						Debug.Log("LatPERFECT");
					}
					GetComponent.<AudioSource>().clip = Success;
					GetComponent(AudioSource).Play();
					LEcount = LEcount + 1;
					Scores_oper.combo = Scores_oper.combo + 1;
					Instantiate(perfecteffect,transform.position,transform.rotation);
					Scores_oper.perfect_count = Scores_oper.perfect_count + 1;
					Scores_oper.score = Scores_oper.score + Scores_oper.score_perfect_perse * Scores_oper.OnenortScore;
					Scores_oper.NortsID_del_Lane3_L = Scores_oper.NortsID_del_Lane3_L + 1;
					Scores_oper.Gauge = Scores_oper.Gauge + Scores_oper.OnenortGauge * Scores_oper.GaugePerfectParse;
					Scores_oper.lastjudge = "Perfect";
					Nowlongmode = false;
				}
			}
		}
	}
	else
	{

	}
}

}