﻿ #pragma strict
var blockPrefab : Transform;
var LS : Transform;
var LSS : Transform;
var LE : Transform;
var Lane4in = new Array();
var Lane4LS = new Array();
var Lane4LE = new Array();
var stf = 0.0f;
var Readcount = 0;
var NortsCount = 0;
var NortsAllcount = 0;
var ShortNortsCount = 0;
var LSCount = 0;
var LECount = 0;
public static var LNTIME = 0.0f;

function Update () 
{
if(Scores_oper.sector == 2)
{
	while(Readcount < ReadOutNorts.Lane4.length)
	{
		Lane4in[Readcount] = ReadOutNorts.Lane4[Readcount];
		Readcount = Readcount + 1;
	}
	Readcount = 0;
	while(Readcount < ReadOutNorts.Lane4LS.length)
	{
		Lane4LS[Readcount] = ReadOutNorts.Lane4LS[Readcount];
		Readcount = Readcount + 1;
	}
	Readcount = 0;
	while(Readcount < ReadOutNorts.Lane4LE.length)
	{
		Lane4LE[Readcount] = ReadOutNorts.Lane4LE[Readcount];
		Readcount = Readcount + 1;
	}
	NortsAllcount = Lane4in.length + Lane4LS.length + Lane4LE.length;
	Scores_oper.sector3_out_Lane4 = true;
}
if(Scores_oper.sector == 3)
{
	if(NortsCount != NortsAllcount)
	{
		if(ShortNortsCount != Lane4in.length)
		{
			stf = float.Parse(Lane4in[ShortNortsCount]) + 0.0f;
			stf = stf / 1000f;
			if(Time.timeSinceLevelLoad >= (stf + Scores_oper.starttime + Scores_oper.waittime)-Scores_oper.just)
			{
				Instantiate(blockPrefab,transform.position,transform.rotation);
				Scores_oper.NortsID_Lane4 = Scores_oper.NortsID_Lane4 + 1; 
				NortsCount++;
				ShortNortsCount++;
			}
		}
		if(LSCount != Lane4LS.length)
		{
			stf = float.Parse(Lane4LS[LSCount]) + 0.0f;
			stf = stf / 1000f;
			if(Time.timeSinceLevelLoad >= (stf + Scores_oper.starttime + Scores_oper.waittime)-Scores_oper.just)
			{
				LNTIME = (60*Scores_oper.Nortsspeed)*(float.Parse(Lane4LE[LSCount])/1000.0f - float.Parse(Lane4LS[LSCount])/1000.0f)*3.2*(17.0f/100.0f);
				Instantiate(LS,transform.position,transform.rotation);
				Instantiate(LSS,transform.position,transform.rotation);
				Scores_oper.NortsID_Lane4_L = Scores_oper.NortsID_Lane4_L + 1; 
				NortsCount++;
				LSCount++;
			}
		}
		if(LECount != Lane4LE.length)
		{
			stf = float.Parse(Lane4LE[LECount]) + 0.0f;
			stf = stf / 1000f;
			if(Time.timeSinceLevelLoad >= (stf + Scores_oper.starttime + Scores_oper.waittime)-Scores_oper.just)
			{
				Instantiate(LE,transform.position,transform.rotation);
				Scores_oper.NortsID_Lane4_LE = Scores_oper.NortsID_Lane4_LE + 1;  
				NortsCount++;
				LECount++;
			}
		}
	}
}
}