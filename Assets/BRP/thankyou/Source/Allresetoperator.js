﻿#pragma strict
var reset = 0;

function Update () 
{	
	if(reset == 0)
	{
		Scores_oper.combo = 0;
		Scores_oper.Maxcombo = 0;
		Scores_oper.lastjudge = "AIR";
		Scores_oper.nextjudge = 0;
		Scores_oper.perfect_count = 0;
		Scores_oper.good_count = 0;
		Scores_oper.bad_count = 0;
		Scores_oper.score = 0;
		Scores_oper.Allnorts = 0;
		Scores_oper.OnenortScore = 0.0f;
		Scores_oper.NortsID_del_Lane1 = 0;
		Scores_oper.NortsID_del_Lane2 = 0;
		Scores_oper.NortsID_del_Lane3 = 0;
		Scores_oper.NortsID_del_Lane4 = 0;
		Scores_oper.NortsID_del_Lane1_L = 0;
		Scores_oper.NortsID_del_Lane2_L = 0;
		Scores_oper.NortsID_del_Lane3_L = 0;
		Scores_oper.NortsID_del_Lane4_L = 0;
		Scores_oper.NortsID_Lane1 = 0;
		Scores_oper.NortsID_Lane2 = 0;
		Scores_oper.NortsID_Lane3 = 0;
		Scores_oper.NortsID_Lane4 = 0;
		Scores_oper.NortsID_Lane1_L = 0;
		Scores_oper.NortsID_Lane2_L = 0;
		Scores_oper.NortsID_Lane3_L = 0;
		Scores_oper.NortsID_Lane4_L = 0;
		Scores_oper.musiclength = 0.0f;
		Scores_oper.starttime = 0.0f;
		Scores_oper.sector3_Lane1 = false;
		Scores_oper.sector3_Lane2 = false;
		Scores_oper.sector3_Lane3 = false;
		Scores_oper.sector3_Lane4 = false;
		Scores_oper.sector3_out_Lane1 = false;
		Scores_oper.sector3_out_Lane2 = false;
		Scores_oper.sector3_out_Lane3 = false;
		Scores_oper.sector3_out_Lane4 = false;
		Scores_oper.sector = 1;
		Scores_oper.musicname = "Musicname-none";
		Scores_oper.selectdifficulty = "Difficulty-none";
		Scores_oper.just = 1.7f;
		Scores_oper.Nortsspeed = 0.1f;
		Scores_oper.Gauge = 20;
		SelectTrans.Music = 1;
		SelectTrans.difficulty = "梅";
		ReadOutNorts.Lane1.Clear();
		ReadOutNorts.Lane2.Clear();
		ReadOutNorts.Lane3.Clear();
		ReadOutNorts.Lane4.Clear();
		ReadOutNorts.Lane1LS.Clear();
		ReadOutNorts.Lane2LS.Clear();
		ReadOutNorts.Lane3LS.Clear();
		ReadOutNorts.Lane4LS.Clear();
		ReadOutNorts.Lane1LE.Clear();
		ReadOutNorts.Lane2LE.Clear();
		ReadOutNorts.Lane3LE.Clear();
		ReadOutNorts.Lane4LE.Clear();
		reset = 1;
	}

}