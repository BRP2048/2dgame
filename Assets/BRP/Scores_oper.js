﻿#pragma strict
//
//					設　定　兄　貴
//

/*定数設定及びグローバル関数の定義専用クラス*/
/*尚一部グローバル関数は直接アクセス       */
/*また、宣言下部には段階中心制御兄貴が在中*/


/*------------------------コンフィグ------------------------*/

/*デバッグモード*/
public static var debug_mode = false;

/*Perfect判定の許容誤差*/
public static var perfectjudge = 0.06f;

/*Good判定の許容誤差*/
public static var goodjudge = 0.08f;

/*Bad判定の開始値*/
public static var badjudge = 0.08f;

/*判定の読み飛ばし値*/
public static var airjudge = 0.08f;

/*LN終点Perfect判定許容誤差*/
public static var LNperfectjudge = 0.15f;

/*LN終点Good判定許容誤差*/
public static var LNgoodjudge = 0.17f;

/*LN終点Bad判定開始値*/
public static var LNbadjudge = 0.17f;

/*LN終点Perfect判定許容誤差*/
public static var LNairjudge = 0.17f;

/*曲開始までの待機時間*/
public static var waittime = 5.0f;

/*判定調整*/
public static var juster = - 0.22f;

/*スコア計算　理論値*/
public static var Maxscore = 1000000.0f;

/*スコア計算　Perfect割合*/
public static var score_perfect_perse = 1.0f;

/*スコア計算　Good割合*/
public static var score_good_perse = 0.5f;

/*スコア計算　Bad割合*/
public static var score_bad_perse = 0.0f;

/*ハイスピ設定 HYPER-SUPER-HIGH-MINIMUMHIGH-NORMAL-MINIMUMLOW-LOW-G-MAGNUMLOW */
public static var Highspeed = "NORMAL";

/*ゲージ加算値　Perfect*/
public static var GaugePerfectParse = 1.0f;

/*ゲージ加算値　Good*/
public static var GaugeGoodParse = 0.5f;

/*ゲージ減算値　Bad*/
public static var GaugeBadParse = -2.0f;

/*曲全体のゲージ値*/
public static var MaxGauge = 1000;

/*オートモード*/
public static var Automode = false;

/*------------------------おしまい------------------------*/



/*------------------------グローバル関数定義------------------------*/
/*値が影響する意味が不明な場合は触らぬこと*/

/*コンボ数格納*/
public static var combo = 0;

/*最大コンボ数*/
public static var Maxcombo = 0;

/*最終判定*/
public static var lastjudge = "AIR";

/*次判定アラート*/
public static var nextjudge = 0;

/*Perfect数カウント*/
public static var perfect_count = 0;

/*Good数カウント*/
public static var good_count = 0;

/*Bad数カウント*/
public static var bad_count = 0;

/*リアルタイムスコア格納*/
public static var score = 0.0f;

/*ノーツ数*/
public static var Allnorts = 0;

/*スコア計算　ノーツ一つの100%計算値*/
public static var OnenortScore = 0.0f;

/*ノーツID　レーンごとの　削除値*/
public static var NortsID_del_Lane1 = 0;

public static var NortsID_del_Lane2 = 0;

public static var NortsID_del_Lane3 = 0;

public static var NortsID_del_Lane4 = 0;

public static var NortsID_del_Lane1_L = 0;

public static var NortsID_del_Lane2_L = 0;

public static var NortsID_del_Lane3_L = 0;

public static var NortsID_del_Lane4_L = 0;

public static var NortsID_del_Lane1_LE = 0;

public static var NortsID_del_Lane2_LE = 0;

public static var NortsID_del_Lane3_LE = 0;

public static var NortsID_del_Lane4_LE = 0;

/*ノーツID割り振り値*/
public static var NortsID_Lane1 = 0;

public static var NortsID_Lane2 = 0;

public static var NortsID_Lane3 = 0;

public static var NortsID_Lane4 = 0;

public static var NortsID_Lane1_L = 0;

public static var NortsID_Lane2_L = 0;

public static var NortsID_Lane3_L = 0;

public static var NortsID_Lane4_L = 0;

public static var NortsID_Lane1_LE = 0;

public static var NortsID_Lane2_LE = 0;

public static var NortsID_Lane3_LE = 0;

public static var NortsID_Lane4_LE = 0;


/*曲長*/
public static var musiclength = 0.0f;

/*読み込み完了タイミング(Time.timeSinceLevelLoad)*/
public static var starttime = 0.0f;

/*セクタ3値成功管理　レーンごとの成功情報*/
public static var sector3_Lane1 = false;

public static var sector3_Lane2 = false;

public static var sector3_Lane3 = false;

public static var sector3_Lane4 = false;

public static var sector3_out_Lane1 = false;

public static var sector3_out_Lane2 = false;

public static var sector3_out_Lane3 = false;

public static var sector3_out_Lane4 = false;

/*段階処理フェーズ値*/
/*停止:0 曲名読み込みに成功:1 譜面解析に成功:2 各オペレータに転送成功:3*/
public static var sector = 1;

/*選択した曲名*/
public static var musicname : String = "Musicname-none";

/*選択した難易度*/
public static var selectdifficulty : String = "Difficulty-none";

/*判定中心値*/
public static var just = 1.7f;

/*ノーツ射出速度*/
public static var Nortsspeed = 0.1f;

/*ゲージ計算　ノーツ一つの100%計算値*/
public static var OnenortGauge = 0.0f;

/*ゲージくん格納値*/
public static var Gauge = 20;

/*------------------------おしまい------------------------*/

//Destroyせんといて
function Awake ()
{
	Application.targetFrameRate = 60;
	DontDestroyOnLoad(this);
}

function Update ()
{
	if(sector3_Lane1 && sector3_Lane2 && sector3_Lane3 && sector3_Lane4 && sector3_out_Lane1 && sector3_out_Lane2 && sector3_out_Lane3 && sector3_out_Lane4 && true)
	{
	if(sector == 2)
		{
			Highspeed = SelectTrans.Speed;
			switch(Highspeed)
			{
			/*各ハイスピ設定のノーツ射出速度と判定中心値の設定*/
				/*case "EXTREME":
				just = 0.3f;
				Nortsspeed = 0.52f;
				break;*/

				case "HYPER":
				just = 0.6f;
				Nortsspeed = 0.42f;
				break;

				case "SUPER":
				just = 0.7f;
				Nortsspeed = 0.32f;
				break;

				case "HIGH":
				just = 0.8f;
				Nortsspeed = 0.27f;
				break;

				case "MINIMUMHIGH":
				just = 1.1f;
				Nortsspeed = 0.17f;
				break;

				case "NORMAL":
				just = 1.7f;
				Nortsspeed = 0.1f;
				break;

				case "MINIMUMLOW":
				just = 1.85f;
				Nortsspeed = 0.09f;
				break;

				case "LOW":
				just = 2.04f;
				Nortsspeed = 0.08f;
				break;

				case "G":
				just = 2.3f;
				Nortsspeed = 0.07f;
				break;

				case "MAGNUMLOW":
				just = 2.65;
				Nortsspeed = 0.06f;
				break;

				default:
				just = 1.7f;
				Nortsspeed = 0.3f;
				break;
			/*おしまい*/
			}
			sector = 3;
			starttime = Time.timeSinceLevelLoad + 0.22f;
		}
	}
}