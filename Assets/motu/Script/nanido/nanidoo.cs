﻿using UnityEngine;
using System.Collections;

public class nanidoo : MonoBehaviour {

    public Texture2D texture;

    public Texture2D startMask, endMask;

    [Range(0, 3)]
    public float fadeinTime = 0.4f, fadeoutTime = 1.4f;

    int nextScene = 0;
    int a = 0;
    int b = 0;
    int c = 1;
    void LoadLevel()
    {
        FadeCamera.Instance.UpdateTexture(texture);
        FadeCamera.Instance.UpdateMaskTexture(startMask);
        FadeCamera.Instance.FadeOut(fadeinTime, () =>
        {
            Application.LoadLevel("nanido");
            FadeCamera.Instance.UpdateMaskTexture(endMask);
            FadeCamera.Instance.FadeIn(fadeoutTime, null);
        });
    }

    public void Update()
    {
        b++;
        if (b == c * 20)
        {
            c++;
      
        }
    }


    // click callback
    public void OnClick()
    {

        if (c >= 8)
        {
            c = 1;
            LoadLevel();
          
        }

    }
}