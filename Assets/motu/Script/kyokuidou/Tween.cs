﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Tween : MonoBehaviour
{
    public EventSystem system;
    public GameObject obj;
    int a, b,c;
    // Use this for initialization
    void Start()
    {
        a = 0;
        b = 0;
        c = 0;
    }
    public void MoveRight()
    {
        if (b == 0 && c == 0 && a == -2)
        {
            iTween.MoveTo(obj, iTween.Hash("x",-2.67, "time", 0.5));
            b = 2;
            return;
        }

        if (a == -2)
        {
            iTween.MoveTo(obj, iTween.Hash("x", 0, "time", 0.5));
            a = 0;
        }
       
        else if (a == 0)
        {
            iTween.MoveTo(obj, iTween.Hash("x", 2.67, "time", 0.5));
            b = 2;
            
        }


    
    }
    public void MoveLeft()
    {
        if (b == 2)
        {
            iTween.MoveTo(obj, iTween.Hash("x", 0, "time", 0.5));
            b = 0;
        }

        else if (b == 0)
        {
            iTween.MoveTo(obj, iTween.Hash("x", -2.67, "time", 0.5));
            a = -2;
            c = 1;
            b = 1;
            return;
             

        }

        if (c == 1)
        {

            iTween.MoveTo(obj, iTween.Hash("x", -5.35, "time", 0.5));
            b = 0;
            c = 0;
        }


    }






    public void Update()
    {
        if (system.currentSelectedGameObject.name == "take" || system.currentSelectedGameObject.name == "matu" || system.currentSelectedGameObject.name == "ume" || system.currentSelectedGameObject.name == "fuzi")
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                MoveLeft();
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                MoveRight();
            }
        }
    }
}
