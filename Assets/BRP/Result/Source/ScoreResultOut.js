﻿#pragma strict
import System;
import System.IO;
import System.Text;

var once = 0;

function Update () {
	if(once == 0)
	{
		var sw:StreamWriter;
    	sw = new StreamWriter(Application.dataPath + "/Resources/result.txt", true);
    	sw.WriteLine(Scores_oper.musicname + ":" + Scores_oper.selectdifficulty + ":" + Scores_oper.score + ";");
    	sw.Close();
		once = once + 1;
	}

}