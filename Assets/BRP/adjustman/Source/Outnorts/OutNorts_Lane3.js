﻿ #pragma strict
var blockPrefab : Transform;
var LS : Transform;
var LSS : Transform;
var LE : Transform;
var Lane3in = new Array();
var Lane3LS = new Array();
var Lane3LE = new Array();
var stf = 0.0f;
var Readcount = 0;
var NortsCount = 0;
var NortsAllcount = 0;
var ShortNortsCount = 0;
var LSCount = 0;
var LECount = 0;
public static var LNTIME = 0.0f;

function Update () 
{
if(Scores_oper.sector == 2)
{
	while(Readcount < ReadOutNorts.Lane3.length)
	{
		Lane3in[Readcount] = ReadOutNorts.Lane3[Readcount];
		Readcount = Readcount + 1;
	}
	Readcount = 0;
	while(Readcount < ReadOutNorts.Lane3LS.length)
	{
		Lane3LS[Readcount] = ReadOutNorts.Lane3LS[Readcount];
		Readcount = Readcount + 1;
	}
	Readcount = 0;
	while(Readcount < ReadOutNorts.Lane3LE.length)
	{
		Lane3LE[Readcount] = ReadOutNorts.Lane3LE[Readcount];
		Readcount = Readcount + 1;
	}
	NortsAllcount = Lane3in.length + Lane3LS.length + Lane3LE.length;
	Scores_oper.sector3_out_Lane3 = true;
}
if(Scores_oper.sector == 3)
{
	if(NortsCount != NortsAllcount)
	{
		if(ShortNortsCount != Lane3in.length)
		{
			stf = float.Parse(Lane3in[ShortNortsCount]) + 0.0f;
			stf = stf / 1000f;
			if(Time.timeSinceLevelLoad >= (stf + Scores_oper.starttime + Scores_oper.waittime)-Scores_oper.just)
			{
				Instantiate(blockPrefab,transform.position,transform.rotation);
				Scores_oper.NortsID_Lane3 = Scores_oper.NortsID_Lane3 + 1; 
				NortsCount++;
				ShortNortsCount++;
			}
		}
		if(LSCount != Lane3LS.length)
		{
			stf = float.Parse(Lane3LS[LSCount]) + 0.0f;
			stf = stf / 1000f;
			if(Time.timeSinceLevelLoad >= (stf + Scores_oper.starttime + Scores_oper.waittime)-Scores_oper.just)
			{
				LNTIME = (60*Scores_oper.Nortsspeed)*(float.Parse(Lane3LE[LSCount])/1000.0f - float.Parse(Lane3LS[LSCount])/1000.0f)*3.2*(17.0f/100.0f);
				Instantiate(LS,transform.position,transform.rotation);
				Instantiate(LSS,transform.position,transform.rotation);
				Scores_oper.NortsID_Lane3_L = Scores_oper.NortsID_Lane3_L + 1; 
				NortsCount++;
				LSCount++;
			}
		}
		if(LECount != Lane3LE.length)
		{
			stf = float.Parse(Lane3LE[LECount]) + 0.0f;
			stf = stf / 1000f;
			if(Time.timeSinceLevelLoad >= (stf + Scores_oper.starttime + Scores_oper.waittime)-Scores_oper.just)
			{
				Instantiate(LE,transform.position,transform.rotation);
				Scores_oper.NortsID_Lane3_LE = Scores_oper.NortsID_Lane3_LE + 1;  
				NortsCount++;
				LECount++;
			}
		}
	}
}
}