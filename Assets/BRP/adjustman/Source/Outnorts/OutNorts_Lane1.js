﻿ #pragma strict
var blockPrefab : Transform;
var LS : Transform;
var LSS : Transform;
var LE : Transform;
var Lane1in = new Array();
var Lane1LS = new Array();
var Lane1LE = new Array();
var stf = 0.0f;
var Readcount = 0;
var NortsCount = 0;
var NortsAllcount = 0;
var ShortNortsCount = 0;
var LSCount = 0;
var LECount = 0;
public static var LNTIME = 0.0f;

function Update () 
{
if(Scores_oper.sector == 2)
{
	while(Readcount < ReadOutNorts.Lane1.length)
	{
		Lane1in[Readcount] = ReadOutNorts.Lane1[Readcount];
		Readcount = Readcount + 1;
	}
	Readcount = 0;
	while(Readcount < ReadOutNorts.Lane1LS.length)
	{
		Lane1LS[Readcount] = ReadOutNorts.Lane1LS[Readcount];
		Readcount = Readcount + 1;
	}
	Readcount = 0;
	while(Readcount < ReadOutNorts.Lane1LE.length)
	{
		Lane1LE[Readcount] = ReadOutNorts.Lane1LE[Readcount];
		Readcount = Readcount + 1;
	}
	NortsAllcount = Lane1in.length + Lane1LS.length + Lane1LE.length;
	Scores_oper.sector3_out_Lane1 = true;
}
if(Scores_oper.sector == 3)
{
	if(NortsCount != NortsAllcount)
	{
		if(ShortNortsCount != Lane1in.length)
		{
			stf = float.Parse(Lane1in[ShortNortsCount]) + 0.0f;
			stf = stf / 1000f;
			if(Time.timeSinceLevelLoad >= (stf + Scores_oper.starttime + Scores_oper.waittime)-Scores_oper.just)
			{
				Instantiate(blockPrefab,transform.position,transform.rotation);
				Scores_oper.NortsID_Lane1 = Scores_oper.NortsID_Lane1 + 1; 
				NortsCount++;
				ShortNortsCount++;
			}
		}
		if(LSCount != Lane1LS.length)
		{
			stf = float.Parse(Lane1LS[LSCount]) + 0.0f;
			stf = stf / 1000f;
			if(Time.timeSinceLevelLoad >= (stf + Scores_oper.starttime + Scores_oper.waittime)-Scores_oper.just)
			{
				LNTIME = (60*Scores_oper.Nortsspeed)*(float.Parse(Lane1LE[LSCount])/1000.0f - float.Parse(Lane1LS[LSCount])/1000.0f)*3.2*(17.0f/100.0f);
				Instantiate(LS,transform.position,transform.rotation);
				Instantiate(LSS,transform.position,transform.rotation);
				Scores_oper.NortsID_Lane1_L = Scores_oper.NortsID_Lane1_L + 1;  
				NortsCount++;
				LSCount++;
			}
		}
		if(LECount != Lane1LE.length)
		{
			stf = float.Parse(Lane1LE[LECount]) + 0.0f;
			stf = stf / 1000f;
			if(Time.timeSinceLevelLoad >= (stf + Scores_oper.starttime + Scores_oper.waittime)-Scores_oper.just)
			{
				Instantiate(LE,transform.position,transform.rotation);
				Scores_oper.NortsID_Lane1_LE = Scores_oper.NortsID_Lane1_LE + 1;  
				NortsCount++;
				LECount++;
			}
		}
	}
}
}