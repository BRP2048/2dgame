﻿using UnityEngine;
using System.Collections;

namespace FCamera
{
    public class FadeSample3 : MonoBehaviour
    {
        public Texture2D texture;

        public Texture2D startMask, endMask;

        [Range(0, 3)]
        public float fadeinTime = 0.4f, fadeoutTime = 1.4f;

        int nextScene = 0;
        int a = 0;
        int b = 0;
        int c = 1;
        /*void Update ()
		{
			if( Input.GetKeyDown(KeyCode.LeftArrow)){
				LoadLevel();
			}
		}*/

        void LoadLevel()
        {
            FadeCamera.Instance.UpdateTexture(texture);
            FadeCamera.Instance.UpdateMaskTexture(startMask);
            FadeCamera.Instance.FadeOut(fadeinTime, () =>
          {
              Application.LoadLevel("thank you");
              FadeCamera.Instance.UpdateMaskTexture(endMask);
              FadeCamera.Instance.FadeIn(fadeoutTime, null);
          });
        }
        void Update()
        {
            b++;
            if (b == c * 20)
            {
                c++;
                
            }

            if (Input.GetKeyDown(KeyCode.Return) && c >= 8)
            {
                LoadLevel();
            }
        }
    }
}
