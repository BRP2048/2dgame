# **No Sweat** #

平成28年度の電子科課題研究でゲーム制作班が制作し、吉報祭での展示及び試遊を行った音楽ゲーム「NoSweat」のGitレポジトリです。

中身はプロジェクトファイルのみであり実行可能ファイルは同梱されていません。実行可能ファイルが必要な場合は資料に記載されているURLへアクセスしてください。

もし、このプロジェクトファイルを参考にして何か課題研究とかで作ったりしたら教えて下さい泣いて喜びます。
　
何か有りましたらCRS@KIRAL.INまで

### - 開発環境###
>Unity 5.3.5f1

### - 使用言語###
>C#
>JavaScript(UnityScript)

### - ライセンス###
>GNU AGPLv3