﻿ #pragma strict
var blockPrefab : Transform;
var LS : Transform;
var LSS : Transform;
var LE : Transform;
var Lane2in = new Array();
var Lane2LS = new Array();
var Lane2LE = new Array();
var stf = 0.0f;
var Readcount = 0;
var NortsCount = 0;
var NortsAllcount = 0;
var ShortNortsCount = 0;
var LSCount = 0;
var LECount = 0;
public static var LNTIME = 0.0f;

function Update () 
{
if(Scores_oper.sector == 2)
{
	while(Readcount < ReadOutNorts.Lane2.length)
	{
		Lane2in[Readcount] = ReadOutNorts.Lane2[Readcount];
		Readcount = Readcount + 1;
	}
	Readcount = 0;
	while(Readcount < ReadOutNorts.Lane2LS.length)
	{
		Lane2LS[Readcount] = ReadOutNorts.Lane2LS[Readcount];
		Readcount = Readcount + 1;
	}
	Readcount = 0;
	while(Readcount < ReadOutNorts.Lane2LE.length)
	{
		Lane2LE[Readcount] = ReadOutNorts.Lane2LE[Readcount];
		Readcount = Readcount + 1;
	}
	NortsAllcount = Lane2in.length + Lane2LS.length + Lane2LE.length;
	Scores_oper.sector3_out_Lane2 = true;
}
if(Scores_oper.sector == 3)
{
	if(NortsCount != NortsAllcount)
	{
		if(ShortNortsCount != Lane2in.length)
		{
			stf = float.Parse(Lane2in[ShortNortsCount]) + 0.0f;
			stf = stf / 1000f;
			if(Time.timeSinceLevelLoad >= (stf + Scores_oper.starttime + Scores_oper.waittime)-Scores_oper.just)
			{
				Instantiate(blockPrefab,transform.position,transform.rotation);
				Scores_oper.NortsID_Lane2 = Scores_oper.NortsID_Lane2 + 1; 
				NortsCount++;
				ShortNortsCount++;
			}
		}
		if(LSCount != Lane2LS.length)
		{
			stf = float.Parse(Lane2LS[LSCount]) + 0.0f;
			stf = stf / 1000f;
			if(Time.timeSinceLevelLoad >= (stf + Scores_oper.starttime + Scores_oper.waittime)-Scores_oper.just)
			{
				LNTIME = (60*Scores_oper.Nortsspeed)*(float.Parse(Lane2LE[LSCount])/1000.0f - float.Parse(Lane2LS[LSCount])/1000.0f)*3.2*(17.0f/100.0f);
				Instantiate(LS,transform.position,transform.rotation);
				Instantiate(LSS,transform.position,transform.rotation);
				Scores_oper.NortsID_Lane2_L = Scores_oper.NortsID_Lane2_L + 1;  
				NortsCount++;
				LSCount++;
			}
		}
		if(LECount != Lane2LE.length)
		{
			stf = float.Parse(Lane2LE[LECount]) + 0.0f;
			stf = stf / 1000f;
			if(Time.timeSinceLevelLoad >= (stf + Scores_oper.starttime + Scores_oper.waittime)-Scores_oper.just)
			{
				Instantiate(LE,transform.position,transform.rotation);
				Scores_oper.NortsID_Lane2_LE = Scores_oper.NortsID_Lane2_LE + 1;  
				NortsCount++;
				LECount++;
			}
		}
	}
}
}