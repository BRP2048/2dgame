﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Tween1 : MonoBehaviour
{
    public EventSystem system;
    public GameObject obj;
    // Use this for initialization
    void Start()
    {

    }
    public void MoveRight()
    {
        if (system.currentSelectedGameObject.name == "matu")
        {
            iTween.MoveTo(obj, iTween.Hash("x",-2.67, "time", 0.5));
           
          
        }

        if (system.currentSelectedGameObject.name == "take")
        {
            iTween.MoveTo(obj, iTween.Hash("x", 0, "time", 0.5));
          
        }
       
        if (system.currentSelectedGameObject.name == "ume")
        {
            iTween.MoveTo(obj, iTween.Hash("x", 2.67, "time", 0.5));
          
            
        }


    
    }
    public void MoveLeft()
    {
        if (system.currentSelectedGameObject.name == "take")
        {
            iTween.MoveTo(obj, iTween.Hash("x", 0, "time", 0.5));
          
        }

       if (system.currentSelectedGameObject.name == "matu")
        {
            iTween.MoveTo(obj, iTween.Hash("x", -2.67, "time", 0.5));
             

        }

        if (system.currentSelectedGameObject.name == "fuzi")
        {

            iTween.MoveTo(obj, iTween.Hash("x", -5.35, "time", 0.5));
        }


    }






    public void Update()
    {
        if (system.currentSelectedGameObject.name == "take" || system.currentSelectedGameObject.name == "matu" || system.currentSelectedGameObject.name == "ume" || system.currentSelectedGameObject.name == "fuzi")
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                MoveLeft();
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                MoveRight();
            }
        }
    }
}
